import logo from "./logo.svg";
import "./App.css";
import Hello from "./Hello";
import ExpenseItem from "./components/ExpenseItem";

function App() {
  const expenses = [
    {
      id: "e1",
      date: new Date(2023, 8, 28),
      title: "Pen",
      description: "Stabilo",
      amount: 1.5,
    },
    {
      id: "e2",
      date: new Date(2023, 8, 28),
      title: "Pencil",
      description: "Stabilo",
      amount: 1.1,
    },
    {
      id: "e3",
      date: new Date(2023, 8, 29),
      title: "Book",
      description: "Mindset",
      amount: 124.45,
    },
    {
      id: "e4",
      date: new Date(2023, 8, 28),
      title: "Calendar",
      description: "Castel",
      amount: 28.34,
    },
  ];

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <ExpenseItem
          date={expenses[0].date}
          title={expenses[0].title}
          description={expenses[0].description}
          amount={expenses[0].amount}
        />
        <ExpenseItem
          date={expenses[1].date}
          title={expenses[1].title}
          description={expenses[1].description}
          amount={expenses[1].amount}
        />
        <ExpenseItem
          date={expenses[2].date}
          title={expenses[2].title}
          description={expenses[2].description}
          amount={expenses[2].amount}
        />
        <ExpenseItem
          date={expenses[3].date}
          title={expenses[3].title}
          description={expenses[3].description}
          amount={expenses[3].amount}
        />
        <Hello />
      </header>
    </div>
  );
}

export default App;
